import setuptools

setuptools.setup(
    name="nn",
    version="19.12.23",
    author="Jonathan",
    author_email="jonathan@pocydon.com",
    description="Fuzzer",
    long_description="Neural network assisted fuzzer",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/twosix-kali/packages1/neuzz",
    packages=["build"],
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)
